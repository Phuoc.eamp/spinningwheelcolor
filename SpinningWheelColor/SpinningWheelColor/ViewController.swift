//  ViewController.swift
//  SpinningWheelColor

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var wheelColorCV: UICollectionView!
    var color: [UIColor] = [.blue,.black,.green,.yellow,.red,.brown]

    override func viewDidLoad() {
        super.viewDidLoad()
        wheelColorCV.dataSource = self
        wheelColorCV.delegate = self
    }

}

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return color.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! WheelCell
        cell.backgroundColor = .clear
        cell.drawColor = color[indexPath.row]
        cell.angle = 2 * .pi / CGFloat(color.count)
        return cell
    }
    
    
}

extension ViewController: UICollectionViewDelegate {
    
}
