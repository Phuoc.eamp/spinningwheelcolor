//
//  WheelColorCVLayoutAttributes.swift
//  SpinningWheelColor
//
//  Created by PhuocNguyen on 6/26/19.
//  Copyright © 2019 PhuocNguyen. All rights reserved.
//

import UIKit

class WheelColorCVLayoutAttributes: UICollectionViewLayoutAttributes {
    var anchorPoint = CGPoint(x: 0.5, y: 0.5)
    var angle: CGFloat = 0 {
        didSet{
            zIndex = Int(angle * 0.1)
            transform = CGAffineTransform(rotationAngle: angle)
        }
    }
    
    override func copy(with zone: NSZone? = nil) -> Any {
        let copyAttributes: WheelColorCVLayoutAttributes = super.copy(with: zone) as! WheelColorCVLayoutAttributes
        copyAttributes.anchorPoint = self.anchorPoint
        copyAttributes.angle = self.angle
        return copyAttributes
    }
    
}
