//
//  wheelCVLayout.swift
//  SpinningWheelColor
//
//  Created by PhuocNguyen on 6/30/19.
//  Copyright © 2019 PhuocNguyen. All rights reserved.
//

import UIKit

class WheelCVLayoutAttrribute: UICollectionViewLayoutAttributes {
    
    var angelForItem: CGFloat = 0
    var anchorPoint: CGPoint = CGPoint(x: 0.5, y: 0.5)
    var angleRotation: CGFloat = 0 {
        didSet {
            zIndex = Int(angleRotation * 100000000)
            transform = CGAffineTransform(rotationAngle: angleRotation)
        }
    }
    override func copy() -> Any {
        let copyAttribute: WheelCVLayoutAttrribute = super.copy() as! WheelCVLayoutAttrribute
        copyAttribute.angleRotation = self.angleRotation
        copyAttribute.anchorPoint = self.anchorPoint
        return copyAttribute
    }
}

class wheelCVLayout: UICollectionViewLayout {
    var listAttributes = [WheelCVLayoutAttrribute]()
    var radius: CGFloat {
        guard let cv = collectionView else {return 0}
        return cv.bounds.height / 2
    }
    
    var anglePerItem: CGFloat {
        guard let cv = collectionView else {return 0}
        return 2 * .pi / CGFloat(cv.numberOfItems(inSection: 0))
    }
    
    var itemSize: CGSize {
        let width = radius * sin(anglePerItem)
        return CGSize(width: width, height: radius)
    }
    
    var angleExtreme: CGFloat {
        guard let cv = collectionView else {return 0}
        return cv.numberOfItems(inSection: 0) > 0 ? CGFloat(cv.numberOfItems(inSection: 0) - 1) * anglePerItem : 0
    }
    
    var angle: CGFloat {
        guard let cv = collectionView else {return 0}
        print("content off: \(cv.contentOffset)")
        return anglePerItem * cv.contentOffset.x / (cv.bounds.width)
        //return angleExtreme * cv.contentOffset.x * 2 * .pi
    }
    
    override var collectionViewContentSize: CGSize {
        guard let cv = collectionView else {return CGSize(width: 0,height: 0)}
        return CGSize(width: radius * 2 * .pi, height: cv.bounds.height)
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    class func layoutAttributesClass() -> Any {
        return WheelCVLayoutAttrribute.self
    }
    
    override func prepare() {
        
        guard let cv = collectionView else {return}
        let centerX = cv.contentOffset.x + cv.bounds.width / 2
        let centerY = cv.contentOffset.y + cv.bounds.height / 2
        
        //let anchorY: CGFloat = itemSize.height / (2 * itemSize.height)
        listAttributes = (0..<cv.numberOfItems(inSection: 0)).map({ (i) -> WheelCVLayoutAttrribute in
            let attribute = WheelCVLayoutAttrribute(forCellWith: IndexPath(item: i, section: 0))
            attribute.size = itemSize
            attribute.center = CGPoint(x: centerX , y: centerY)
            
            attribute.angleRotation = -anglePerItem / 2 + anglePerItem * CGFloat(i) 
            attribute.angelForItem = anglePerItem
            attribute.anchorPoint = CGPoint(x: 0, y: 1)
            return attribute
        })
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return listAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return listAttributes[indexPath.row]
    }
}

class WheelCell: UICollectionViewCell {
    
    var drawColor: UIColor = .orange {
        didSet {
            layoutIfNeeded()
        }
    }
    
    var angle: CGFloat = 0 {
        didSet {
            layoutIfNeeded()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        let attribute = layoutAttributes as! WheelCVLayoutAttrribute
        self.layer.anchorPoint = attribute.anchorPoint
        self.angle = attribute.angelForItem
    }
    
    override func draw(_ rect: CGRect) {
        drawColor.setFill()
        let startAngle: CGFloat = -1 * .pi / 2
        let endAngle: CGFloat = startAngle + angle
        let point: CGPoint = CGPoint(x: 0, y: rect.height)
        let midPath = UIBezierPath()
        midPath.move(to: point)
        midPath.addArc(withCenter: point, radius: rect.height, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        midPath.close()
        midPath.fill()

    }
    
    
    
    
}
