//
//  WheelColorCVLayout.swift
//  SpinningWheelColor
//

import UIKit

class WheelColorCVLayout: UICollectionViewLayout {

    let itemSize = CGSize(width: 40, height: 40)
    var radius: CGFloat {
        guard let cv = collectionView else{return 0}
        return 100
    }
    
    var anglePerItem: CGFloat {
        return atan(itemSize .width / radius)
    }
    
    var angleExtreme: CGFloat {
        guard let cv = collectionView else {return 0}
        return cv.numberOfItems(inSection: 0) > 0 ? -CGFloat(cv.numberOfItems(inSection: 0) - 1) * anglePerItem : 0
    }
    
    var angle: CGFloat {
        guard let cv = collectionView else {return 0}
        print("content off: \(cv.contentOffset.x)")
//        return angleExtreme * cv.contentOffset.x / (collectionViewContentSize.width -
//            cv.bounds.width)
        return angleExtreme * cv.contentOffset.x
    }
    var listAttributes = [WheelColorCVLayoutAttributes]()
    
    
    override var collectionViewContentSize: CGSize {
        guard let collectionView = collectionView else {return CGSize.zero}
        return CGSize(width: CGFloat(collectionView.numberOfItems(inSection: 0)) * itemSize.width, height: collectionView.bounds.height)
    }
    
    class func layoutAttributesClass() -> Any {
        return WheelColorCVLayoutAttributes.self
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return listAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return listAttributes[indexPath.row]
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    override func prepare() {
        super.prepare()
        guard let cv = collectionView else {return}
        let centerX = cv.contentOffset.x + cv.bounds.width / 2
        let anchorPointY = ((itemSize.height / 2.0) + radius) / itemSize.height
        let theta = atan2(cv.bounds.width / 2, radius + itemSize.height/2 - cv.bounds.height/2)
        let centerY = UIScreen.main.bounds.height - cv.bounds.height
        print("center Y: \(centerY)")
        var startIndex = 0
        var endIndex = cv.numberOfItems(inSection: 0) - 1
//        if angle < -theta {
//            startIndex = Int(floor((-theta - angle) / anglePerItem))
//        }
//        if endIndex < startIndex {
//            endIndex = 0
//            startIndex = 0
//        }
        print("center cv: \(cv.center)")
        listAttributes = (startIndex...endIndex).map { (i) -> WheelColorCVLayoutAttributes in
            let attributes = WheelColorCVLayoutAttributes(forCellWith: IndexPath(row: i, section: 0))
            attributes.size = self.itemSize
            attributes.center = CGPoint(x: centerX, y: cv.center.y)
            attributes.anchorPoint = CGPoint(x: 0.5, y: anchorPointY)
            attributes.angle = self.angle + self.anglePerItem * CGFloat(i)
            return attributes
        }
    }
    
}
