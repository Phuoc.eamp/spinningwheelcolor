//
//  WheelColorCollectionCell.swift
//  SpinningWheelColor
//

import UIKit

class WheelColorCollectionCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        self.isOpaque = true
        self.alpha = 0.7
        let attribute = layoutAttributes as! WheelColorCVLayoutAttributes
        self.layer.anchorPoint = attribute.anchorPoint
        self.center.y += (attribute.anchorPoint.y - 0.5) * self.bounds.height
    }
    
}
